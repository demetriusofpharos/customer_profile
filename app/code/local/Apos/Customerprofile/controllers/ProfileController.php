<?php
class Apos_Customerprofile_ProfileController extends Mage_Core_Controller_Front_Action {    
  public function indexAction()
  {
		$this->loadLayout();
		$this->getLayout()->getBlock('head')->setTitle(Mage::app()->getStore()->getFrontendName() . " - " . $this->__('Customer Profile'));
    $this->renderLayout();
  }
}  
