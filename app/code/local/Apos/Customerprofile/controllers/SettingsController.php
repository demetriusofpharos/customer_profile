<?php
class Apos_Customerprofile_SettingsController extends Mage_Core_Controller_Front_Action {
  /**
   * 
   */
  public function preDispatch()
  {
    parent::preDispatch();
    $action = $this->getRequest()->getActionName();
    $loginUrl = Mage::helper('customer')->getLoginUrl();

    if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
      $this->setFlag('', self::FLAG_NO_DISPATCH, true);
    }
  }


  /**
   *
   */
  public function indexAction()
  {
    $this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		$this->getLayout()->getBlock('head')->setTitle(Mage::app()->getStore()->getFrontendName() . " - " . $this->__('Profile Settings'));
    $this->renderLayout();
  }


  /**
   *
   */
	public function saveAction()
	{
		if ( $this->getRequest()->getPost() ) {
			$customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
      $postData = $this->getRequest()->getPost(); // get all post data

      if ($_FILES['photo_upload']['name'] !== '') {
        $uploadedImage = $this->uploadForm();
      }
      try
      {
        $profile = Mage::getModel('customerprofile/profile')->load($customerId, 'customer_id');
        $custom_url = $postData['custom_url'];
        $status = ((isset($postData['status'])) ? 1 : 0);
        $custom_url = preg_replace('/[^a-zA-Z0-9]+/', '', $custom_url);
        $custom_url = urlencode($custom_url);
        $isavailable = Mage::getModel('customerprofile/profile')->load($custom_url, 'custom_url');
        
        if($isavailable->getCustomUrl() == "")
        {
          $rewrite = Mage::getModel('core/url_rewrite'); 
          $store_id = Mage::app()->getStore()->getStoreId();
          $route_exist = Mage::getModel('core/url_rewrite')->getCollection()->addFieldToFilter('request_path','customerprofile/' . $custom_url . '.html');
          if(count($route_exist) <= 0)
          {
            $rewrite->setStoreId($store_id)->setIdPath('customerprofile/' . $custom_url)->setRequestPath('customerprofile/' . $custom_url . '.html')->setTargetPath('customerprofile/profile/index/?u=' . $custom_url)->setIsSystem(true)->save();
          }
        }

        if($custom_url == "")
        {
          $status = 0;
        }
        
        if($isavailable->getCustomUrl() == "" || $isavailable->getCustomUrl() == $profile->getCustomUrl()) 
        {
          if($profile->getCustomerId() != "")
          {
            $data = array('custom_url' => $custom_url,
                          //'customer_id' => $customerId,
                          'profile_img' => ($uploadedImage != "") ? $uploadedImage : $postData['profile_img'],
                          'status' => $status
                         );
            $model = Mage::getModel('customerprofile/profile')->load($profile->getId())->addData($data);
            $model->setId($profile->getId())->save();
          }
          else
          {
            Mage::getModel('customerprofile/profile')->setId($this->getRequest()->getParam('entity_id'))
              ->setCustomerId($customerId)
              ->setCustomUrl($custom_url)
              ->setProfileImg(($uploadedImage != "") ? $uploadedImage : $postData['profile_img'])
              ->setStatus($status)
              ->save();
          }
          Mage::getSingleton('customer/session')->addSuccess(Mage::helper('customerprofile')->__('Profile was successfully saved'));
        }
        else
        {
          Mage::getSingleton('customer/session')->addError(Mage::helper('customerprofile')->__('This Custom URL Is Already Been Used By Someone Else. Please Try Another One'));
        }
        $this->_redirect('customerprofile/settings/index');
        return;
      } catch (Exception $e) {
        Mage::getSingleton('customer/session')->addError($e->getMessage());
        Mage::getSingleton('customer/session')->setLocalshipData($this->getRequest()->getPost());
        Mage::getSingleton('customer/session')->addError(Mage::helper('customerprofile')->__('Error'));
        $this->_redirect('customerprofile/settings/index', array('entity_id' => $this->getRequest()->getParam('entity_id')));
        return;
      }
		}
		$this->_redirect('customerprofile/settings/index');
  }

  /**
   *
   */
  public function uploadForm(){		
		$_helper = Mage::helper('customerprofile');
		$base_path = Mage::getBaseDir('media'); 
    //$customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
    $types = array(
      "image/jpeg" => "jpg",
      "image/png"  => "png",
      "image/gif"  => "gif",
    );

    $photo    = $_FILES['photo_upload'];
		$src_file = $photo['tmp_name'];
		$src_path = $photo['name'];		
    //$ext = pathinfo($src_file, PATHINFO_EXTENSION);

    if (is_file($src_file) && in_array($photo['type'], array_keys($types))) {
      $ext = $types[$photo['type']];
			$thumbw = $_helper->getThumbW();	
			$thumbh = $_helper->getThumbH();				
			
			if (!file_exists($base_path.'/profile/thumbs/'.$thumbw)) { 				
				mkdir($base_path.'/profile/thumbs/'.$thumbw, 0777, true); 
			}			 
    
			$img_name = $_helper->generateImgName($ext);
			$img_dest = $base_path.'/profile/'.$img_name;
			copy($src_file, $img_dest);
			
			//echo '<script type="text/javascript">window.top.window.show_popup_crop("'.$img_dest.'","'.$customer_id.'","'.$thumbw.'","'.$thumbh.'")</script>';
      
      //$crop = $this->cropAction($img_dest);
      //return $crop;
      return $img_dest;
    }
    else {
      Mage::getSingleton('customer/session')->addError(Mage::helper('customerprofile')->__('This image cannot be uploaded at this time. Please try again, possibly with a different image.'));
      return false;
    }
  }


  /**
   *
   */
  public function cropAction($img_path){	
    $_helper    = Mage::helper('customerprofile');
		/*$root_path  = Mage::getBaseDir();  
		$media_url  = Mage::getBaseUrl('media');
		
		$postData   = $this->getRequest()->getPost(); 
		$targ_w     = $postData['targ_w'];
		$targ_h     = $postData['targ_h'];
    
    $thumbW     = $_helper->getThumbW();
		$thumbH     = $_helper->getThumbH();
		
		$src 	      = $postData['photo_upload'];
		$img_r 	    = $_helper->createThumbnail($img_path);

    $data = array('custom_url' => $custom_url,
                  //'customer_id' => $customerId,
                  'profile_img' => $img_r,
                  'status' => $status
                );*/

		$img_r 	    = $_helper->createThumbnail($img_path);
		
		return $img_r;
	}	
}
