<?php

$installer = $this;
$installer->startSetup();

$installer->run(" 
	-- DROP TABLE IF EXISTS {$this->getTable('apos_customerprofile')};
	CREATE TABLE {$this->getTable('apos_customerprofile')} (
	  `entity_id` int(11) unsigned NOT NULL auto_increment,
	  `customer_id` int(11) NULL,
	  `custom_url` varchar(255) NULL,
	  `profile_img` varchar(255) NULL,
	  `status` smallint(1) NOT NULL default '0',
	  key (entity_id)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  ")

  /*$route_exist = Mage::getModel('core/url_rewrite')->getCollection()->addFieldToFilter('request_path','customerprofile/settings/upload');
  if(count($route_exist) <= 0)
  {
    $rewrite->setStoreId($store_id)->setIdPath('customerprofile/' . $custom_url)->setRequestPath('customerprofile/settings/upload')->setTargetPath('customerprofile/settings/upload')->setIsSystem(true)->save();
  }*/

  $installer->endSetup();

  /**
   *
CREATE TABLE `apos_customerprofile` (
  `entity_id` int(11) unsigned NOT NULL auto_increment,
  `customer_id` int(11) NULL,
  `custom_url` varchar(255) NULL,
  `profile_img` varchar(255) NULL,
  `status` smallint(1) NOT NULL default '0',
  key (entity_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  */
