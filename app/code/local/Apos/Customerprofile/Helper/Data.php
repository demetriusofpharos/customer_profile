<?php
class Apos_Customerprofile_Helper_Data extends Mage_Core_Helper_Abstract
{
  /**
   *
   */
  public function getThumbW()
  {
		$thumbw = Mage::getStoreConfig('customerprofile/profilepic/img_width');
		$thumbw = (isset($thumbw) && $thumbw != '') ? $thumbw : 200;
		return $thumbw;
  }


  /**
   *
   */
	public function getThumbH()
    {
		$thumbh = Mage::getStoreConfig('customerprofile/profilepic/img_height');
		$thumbh = (isset($thumbh) && $thumbh != '') ? $thumbh : 200;
		return $thumbh;
  }


  /**
   *
   */
  public function getImgName($src)
  {
		return preg_replace('/^.+[\\\\\\/]/', '', $src);
	}


  /**
   *
   */
  public function generateImgName($ext)
  {
		if(Mage::getSingleton('customer/session')->isLoggedIn()) {
			$customerData = Mage::getSingleton('customer/session')->getCustomer();
			$customer_id =  $customerData->getId();
		}		
    return 'customer_profile_'.time().'_'.$customer_id.".".$ext;
  }


  /**  
	 *  
	 */
	public function createThumbnail($filename) {
    $path       = explode('media/', $filename);
    $fullpath   = Mage::getBaseDir() . '/media/' . $path[1];
    $info 			= pathinfo($fullpath);
		$ext        = strtolower($info['extension']);
		$extensions = array("png", "jpg", "gif");
    $thumbname  = $info['dirname']."/thumbs/".$this->getThumbW().'/'.$info['basename'];
    $baseurl    = Mage::getBaseURL('media').'profile/'.$info['basename'];
    $thumburl   = Mage::getBaseURL('media').'profile/thumbs/'.$this->getThumbW().'/'.$info['basename'];

		if(is_file($fullpath) && in_array($ext, $extensions)) {
      switch($ext) {
        case 'png':
          $originalIm = imagecreatefrompng($fullpath);
          break;

        case 'gif':
			  	$originalIm = imagecreatefromgif($fullpath);
          break;

        case 'jpg';
        case 'jpeg';
        default;
          $originalIm = imagecreatefromjpeg($fullpath);
          break;
      }

			$originalX = imagesx($originalIm);
			$originalY = imagesy($originalIm);
	
			$thumbTrue = imagecreatetruecolor($this->getThumbW(), $this->getThumbH());
		
			imagecopyresized($thumbTrue, $originalIm, 0, 0, 0, 0, $this->getThumbW(), $this->getThumbH(), $originalX, $originalY);

      switch($ext) {
        case 'png':
          imagejpeg($thumbTrue, $thumbname);
          break;

        case 'gif':
			  	imagegif($thumbTrue, $thumbname);
          break;

        case 'jpg';
        case 'jpeg';
        default;
          imagepng($thumbTrue, $thumbname);
          break;
      }
			
			imagedestroy($thumbTrue);
			imagedestroy($originalIm);
    }
    else {
      Mage::getSingleton('customer/session')->addError(Mage::helper('customerprofile')->__('This image cannot be uploaded at this time. Please try again, possibly with a different image.'));
      return false;
    }

    return (is_file($thumbname)) ? $thumburl : $baseurl ;
	}
}
